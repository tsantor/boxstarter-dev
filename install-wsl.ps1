# Windows Subsystems/Features
$confirmation = Read-Host "Install Windows Subsystems/Features?"
if ($confirmation -eq 'y') {
    # cinst Microsoft-Hyper-V-All -source windowsFeatures
    cinst Microsoft-Windows-Subsystem-Linux -source windowsfeatures

    # Download and install Ubuntu
    Invoke-WebRequest -Uri https://aka.ms/wsl-ubuntu-1604 -OutFile ~/Ubuntu.appx -UseBasicParsing
    Add-AppxPackage -Path ~/Ubuntu.appx
}
