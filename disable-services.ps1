# This script disables unwanted Windows services. If you do not want to disable
# certain services comment out the corresponding lines below.

# $ErrorActionPreference = 'SilentlyContinue'

Write-Output "--------------------------------------------------------------------------------"
Write-Output "Setting specific services to Disabled..."

$services = @(
    "ALG" # Application Layer Gateway Service
    "tzautoupdate" # Auto Time Zone Updater
    "PeerDistSvc" # BranchCache
    "NfsClnt" # Client for NFS
    "dmwappushsvc" # dmwappushsvc
    "MapsBroker" # Downloaded Maps Manager
    "lfsvc" # Geolocation Service
    "HvHost" # HV Host Service
    "vmickvpexchange" # Hyper-V Data Exchange Service
    "vmicguestinterface" # Hyper-V Guest Service Interface
    "vmicshutdown" # Hyper-V Guest Shutdown Service
    "vmicheartbeat" # Hyper-V Heartbeat Service
    "vmicvmsession" # Hyper-V PowerShell Direct Service
    "vmicrdv" # Hyper-V Remote Desktop Virtualization Service
    "vmictimesync" # Hyper-V Time Synchronization Service
    "vmicvss" # Hyper-V Volume Shadow Copy Requestor
    "irmon" # Infrared monitor service
    "SharedAccess" # Internet Connection Sharing (ICS)
    "AppVClient" # Microsoft App-V Client
    "MSiSCSI" # Microsoft iSCSI Initiator Service
    "SmsRouter" # Microsoft Windows SMS Router Service
    "CscService" # Offline Files
    "WpcMonSvc" # Parental Controls
    "SEMgrSvc" # Payments and NFC/SE Manager
    "PhoneSvc" # Phone Service
    "RpcLocator" # Remote Procedure Call (RPC) Locator
    "RemoteRegistry" # Remote Registry
    "RetailDemo" # Retail Demo Service
    "RemoteAccess" # Routing and Remote Access
    "SensorDataService" # Sensor Data Service
    "SensrSvc" # Sensor Monitoring Service
    "SensorService" # Sensor Service
    "shpamsvc" # Shared PC Account Manager
    "SCardSvr" # Smart Card
    "ScDeviceEnum" # Smart Card Device Enumeration Service
    "SCPolicySvc" # Smart Card Removal Policy
    "SNMPTRAP" # SNMP Trap
    "TabletInputService" # Touch Keyboard and Handwriting Panel Service
    "UevAgentService" # User Experience Virtualization Service
    "WFDSConSvc" # Wi-Fi Direct Services Connection Manager Service
    "FrameServer" # Windows Camera Frame Server
    "wisvc" # Windows Insider Service
    "WMPNetworkSvc" # Windows Media Player Network Sharing Service **
    "icssvc" # Windows Mobile Hotspot Service
    "WinRM" # Windows Remote Management (WS-Management)
    "WwanSvc" # WWAN AutoConfig
    "XblAuthManager" # Xbox Live Auth Manager
    "XblGameSave" # Xbox Live Game Save
    "XboxNetApiSvc" # Xbox Live Networking Service
)

foreach ($service in $services) {
    # Write-Output "Trying to disable $service"
    # Get-Service -Name $service | Set-Service -StartupType Disabled

    If (Get-Service $service -ErrorAction SilentlyContinue) {
        If ((Get-Service $service).Status -eq 'Running') {
            Stop-Service $service
            Write-Host "Stopping $service"
            Get-Service -Name $service | Set-Service -StartupType Disabled
        }
        # Else {
        #     Write-Warning "$service found, but it is not running."
        # }
    } Else {
        Write-Warning "$service not found"
    }
}


Write-Output "--------------------------------------------------------------------------------"
Write-Output "Setting specific services to Manual..."

$manual_services = @(
    "AxInstSV" # ActiveX Installer (AxInstSV)
    "AJRouter" # AllJoyn Router Service
    "AppReadiness" # App Readiness
    # "AppIDSvc" # Application Identity
    # "Appinfo" # Application Information
    "AppMgmt" # Application Management
    # "AppXSVC" # AppX Deployment Service (AppXSVC)
    "AssignedAccessManagerSvc" # AssignedAccessManager Service
    "BthAvctpSvc" # AVCTP service
    "BDESVC" # BitLocker Drive Encryption Service
    "wbengine" # Block Level Backup Engine Service
    "BTAGService" # Bluetooth Audio Gateway Service
    "bthserv" # Bluetooth Support Service
    "BluetoothUserService_?????" # Bluetooth User Support Service_?????
    "camsvc" # Capability Access Manager Service
    "CaptureService_?????" # CaptureService_?????
    "CertPropSvc" # Certificate Propagation
    # "ClipSVC" # Client License Service (ClipSVC)
    "KeyIso" # CNG Key Isolation
    "COMSysApp" # COM+ System Application
    "Browser" # Computer Browser
    # "PimIndexMaintenanceSvc_?????" # Contact Data_?????
    "VaultSvc" # Credential Manager
    "DsSvc" # Data Sharing Service
    "DeviceAssociationService" # Device Association Service
    "DeviceInstall" # Device Install Service
    "DmEnrollmentSvc" # Device Management Enrollment Service
    "DsmSVC" # Device Setup Manager
    "DevicePickerUserSvc_27845" # DevicePicker_27845
    "DevicesFlowUserSvc_?????" # DevicesFlow_?????
    "DevQueryBroker" # DevQuery Background Discovery Broker
    "diagsvc" # Diagnostic Execution Service
    "WdiServiceHost" # Diagnostic Service Host
    "WdiSystemHost" # Diagnostic System Host
    "MSDTC" # Distributed Transaction Coordinator
    "embeddedmode" # Embedded Mode
    "EFS" # Encrypting File System (EFS)
    "EntAppSvc" # Enterprise App Management Service
    "EapHost" # Extensible Authentication Protocol Service
    "fhsvc" # File History Service
    # "fdPHost" # Function Discovery Provider Host
    # "FDResPub" # Function Discovery Resource Publication
    "BcastDVRUserService_?????" # GameDVR and Broadcast User Service_28eed
    "GraphicsPerfSvc" # GraphicsPerfSvc
    "hidserv" # Human Interface Device Service
    "IKEEXT" # IKE and AuthIP IPsec Keying Modules
    "UI0Detect" # Interactive Services Detection
    "IpxlatCfgSvc" # IP Translation Configuration Service
    "PolicyAgent" # IPsec Policy Agent
    "KtmRm" # KtmRm for Distributed Transaction Coordinator
    "lltdsvc" # Link-Layer Topology Discovery Mapper
    "wlpasvc" # Local Profile Assistant Service
    "MessagingService_?????" # MessagingService_?????
    "diagnosticshub.standardcollector.service" # Microsoft (R) Diagnostics Hub Standard Collector Service
    "wlidsvc" # Microsoft Account Sign-in Assistant
    # "NgcSvc" # Microsoft Passport
    # "NgcCtnrSvc" # Microsoft Passport Container
    "swprv" # Microsoft Software Shadow Copy Provider
    "smphost" # Microsoft Storage Spaces SMP
    "InstallService" # Microsoft Store Install Service
    "NaturalAuthentication" # Natural Authentication
    "Netlogon" # Netlogon
    "NcdAutoSetup" # Network Connected Devices Auto-Setup
    # "NcbService" # Network Connection Broker
    "Netman" # Network Connections
    "NcaSVC" # Network Connectivity Assistant
    # "netprofm" # Network List Service
    "NetSetupSvc" # Network Setup Service
    "ssh-agent" # OpenSSH Authentication Agent
    "defragsvc" # Optimize Drives
    "PNRPsvc" # Peer Name Resolution Protocol
    "p2psvc" # Peer Networking Grouping
    "p2pimsvc" # Peer Networking Identity Manager
    "PerfHost" # Performance Counter DLL Host
    "pla" # Performance Logs & Alerts
    "PlugPlay" # Plug and Play
    "PNRPAutoReg" # PNRP Machine Name Publication Service
    "WPDBusEnum" # Portable Device Enumerator Service
    "PrintNotify" # Printer Extensions and Notifications
    "PrintWorkflowUserSvc_?????" # PrintWorkflow_?????
    "wercplsupport" # Problem Reports and Solutions Control Panel Support
    "QWAVE" # Quality Windows Audio Video Experience
    # "RmSvc" # Radio Management Service
    "RasAuto" # Remote Access Auto Connection Manager
    "RasMan" # Remote Access Connection Manager
    "SessionEnv" # Remote Desktop Configuration
    "TermService" # Remote Desktop Services
    "UmRdpService" # Remote Desktop Services UserMode Port Redirector
    "seclogon" # Secondary Logon
    # "SstpSvc" # Secure Socket Tunneling Protocol Service
    "SharedRealitySvc" # Spatial Data Service
    "svsvc" # Spot Verifier
    "SSDPSRV" # SSDP Discovery
    # "StateRepository" # State Repository Service
    "WiaRpc" # Still Image Acquisition Events
    "StorSvc" # Storage Service
    "TieringEngineService" # Storage Tiers Management
    "lmhosts" # TCP/IP NetBIOS Helper
    "TapiSrv" # Telephony
    "TimeBroker" # Time Broker
    # "UsoSvc" # Update Orchestrator Service for Windows Update
    "upnphost" # UPnP Device Host
    # "UserDataSvc_?????" # User Data Access_?????
    # "UnistoreSvc_?????" # User Data Storage_?????
    "vds" # Virtual Disk
    "VSS" # Volume Shadow Copy
    "WalletService" # WalletService
    "TokenBroker" # Web Account Manager
    "WebClient" # WebClient
    "SDRSVC" # Windows Backup
    "WbioSrvc" # Windows Biometric Service
    "wcncsvc" # Windows Connect Now - Config Registrar
    # "Sense" # Windows Defender Advanced Threat Protection Service
    # "WdNisSvc" # Windows Defender Antivirus Network Inspection Service
    "WEPHOSTSVC" # Windows Encryption Provider Host Service
    "WerSvc" # Windows Error Reporting Service
    "Wecsvc" # Windows Event Collector
    "StiSvc" # Windows Image Acquisition (WIA)
    "msiserver" # Windows Installer
    "LicenseManager" # Windows License Manager Service
    "TrustedInstaller" # Windows Modules Installer
    "spectrum" # Windows Perception Service
    "PushToInstall" # Windows PushToInstall Service
    "W32Time" # Windows Time
    "wuauserv" # Windows Update
    "WaaSMedicSvc" # Windows Update Medic Service
    # "WinHttpAutoProxySvc" # WinHTTP Web Proxy Auto-Discovery Service
    "dot3svc" # Wired AutoConfig
    # "WlanSvc" # WLAN AutoConfig - you'll have no Wi-Fi!
    "wmiApSrv" # WMI Performance Adapter
    "XboxGipSvc" # Xbox Accessory Management Service
    "xbgm" # Xbox Game Monitoring
)

foreach ($service in $manual_services) {
    #Write-Output "Trying to manual $service"
    # Get-Service -Name $service | Set-Service -StartupType Manual

    If (Get-Service $service -ErrorAction SilentlyContinue) {
        If ((Get-Service $service).Status -eq 'Running') {
            Stop-Service $service
            Write-Host "Stopping $service"
            Get-Service -Name $service | Set-Service -StartupType Manual
        }
        # Else {
        #     Write-Warning "$service found, but it is not running."
        # }
    } Else {
        Write-Warning "$service not found"
    }
}


Write-Output "--------------------------------------------------------------------------------"
Write-Output "Setting specific services to Disabled..."
$misc_services = @(
    # Adobe Acrobat Update Service
    "BthHFSrv" # Bluetooth Handsfree Service
    # Internet Explorer ETW Collector Service
    "Spooler" # Print Spooler
    # Skype Updater
)

foreach ($service in $misc_services) {
    #Write-Output "Trying to disable $service"
    # Get-Service -Name $service | Set-Service -StartupType Disabled

    If (Get-Service $service -ErrorAction SilentlyContinue) {
        If ((Get-Service $service).Status -eq 'Running') {
            Stop-Service $service
            Write-Host "Stopping $service"
            Get-Service -Name $service | Set-Service -StartupType Disabled
        }
        # Else {
        #     Write-Warning "$service found, but it is not running."
        # }
    } Else {
        Write-Warning "$service not found"
    }
}
