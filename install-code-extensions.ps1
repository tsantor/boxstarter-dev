Write-Output "--------------------------------------------------------------------------------"
Write-Output "Install Visual Studio Code extensions..."

# need to launch vscode so user folders are created as we can install extensions
$process = Start-Process code -PassThru
Start-Sleep -s 10
$process.Close()

code --install-extension abusaidm.html-snippets
code --install-extension alefragnani.project-manager
code --install-extension BattleBas.kivy-vscode
code --install-extension bibhasdn.django-html
code --install-extension christian-kohler.npm-intellisense
code --install-extension darkriszty.markdown-table-prettify
code --install-extension dbaeumer.jshint
code --install-extension donjayamanne.githistory
code --install-extension DotJoshJohnson.xml
code --install-extension EditorConfig.EditorConfig
code --install-extension emmanuelbeziat.vscode-great-icons
code --install-extension gerane.Theme-Brogrammer
code --install-extension gerane.Theme-FlatlandMonokai
code --install-extension GrapeCity.gc-excelviewer
code --install-extension ionutvmi.reg
code --install-extension JerryHong.autofilename
code --install-extension joelday.docthis
code --install-extension kokororin.vscode-phpfmt
code --install-extension lextudio.restructuredtext
code --install-extension magicstack.MagicPython
code --install-extension marcostazi.VS-code-vagrantfile
code --install-extension mikestead.dotenv
code --install-extension mrmlnc.vscode-csscomb
code --install-extension ms-python.python
code --install-extension ms-vscode.PowerShell
code --install-extension steve8708.Align
code --install-extension tht13.python
code --install-extension Tyriar.sort-lines
code --install-extension wholroyd.jinja
code --install-extension yopsolo.ActionScript3
code --install-extension yzane.markdown-pdfcode
code --install-extension Zignd.html-css-class-completion
