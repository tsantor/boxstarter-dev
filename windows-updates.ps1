# Update Windows and reboot if necessary
Write-Output "Enable MicrosoftUpdate"
Enable-MicrosoftUpdate
$confirmation = Read-Host "Install Windows Updates? This can take a while. Are you sure?"
if ($confirmation -eq 'y') {
    Write-Output "Installing Windows Updates..."
    Install-WindowsUpdate -AcceptEula -GetUpdatesFromMS -SuppressReboots
}

Write-Output "Disable MicrosoftUpdate"
Disable-MicrosoftUpdate
