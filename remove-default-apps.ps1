Write-Output "--------------------------------------------------------------------------------"
Write-Output "Uninstall unecessary applications..."
# Get-AppxPackage -AllUsers
# Get-AppxPackage -AllUsers | Select Name
# Get-AppxProvisionedPackage -Online | Format-Table DisplayName, PackageName

# Get Windows Features
# DISM /online /get-features /format:table | more
# DISM /online /get-features /format:table | findstr IIS
# choco list --source windowsFeatures

$apps = @(
    # Default Windows 10 apps
    "Microsoft.3DBuilder"
    "Microsoft.3DScan"
    "Microsoft.AppConnector"
    "Microsoft.AppInstaller"
    "Microsoft.BingFinance"
    "Microsoft.BingNews"
    "Microsoft.BingSports"
    "Microsoft.BingTranslator"
    "Microsoft.BingWeather"
    "Microsoft.CommsPhone"
    "Microsoft.ConnectivityStore"
    "Microsoft.GetHelp"
    "Microsoft.Getstarted"
    "Microsoft.MSPaint"
    "Microsoft.Messaging"
    "Microsoft.Microsoft3DViewer"
    "Microsoft.MicrosoftOfficeHub"
    "Microsoft.MicrosoftPowerBIForWindows"
    "Microsoft.MicrosoftSkype"
    "Microsoft.MicrosoftSolitaireCollection"
    "Microsoft.MicrosoftStickyNotes"
    "Microsoft.MinecraftUWP"
    "Microsoft.NetworkSpeedTest"
    "Microsoft.Office.Desktop"
    "Microsoft.Office.OneNote"
    "Microsoft.Office.Sway"
    "Microsoft.OneConnect"
    "Microsoft.People"
    "Microsoft.Print3D"
    "Microsoft.SkreenSketch"
    "Microsoft.SkypeApp"
    "Microsoft.Wallet"
    "Microsoft.Windows.Photos"
    "Microsoft.WindowsAlarms"
    "Microsoft.WindowsCamera"
    "Microsoft.WindowsFeedbackHub"
    "Microsoft.WindowsMaps"
    "Microsoft.WindowsPhone"
    "Microsoft.WindowsSoundRecorder"
    "Microsoft.XboxApp"
    "Microsoft.XboxGameOverlay"
    "Microsoft.XboxIdentityProvider"
    "Microsoft.XboxSpeechToTextOverlay"
    "Microsoft.ZuneMusic"
    "Microsoft.ZuneVideo"
    # "microsoft.windowscommunicationsapps"

    # App which cannot be removed using Remove-AppxPackage
    #"Microsoft.BioEnrollment"
    #"Microsoft.MicrosoftEdge"
    #"Microsoft.Windows.Cortana"
    #"Microsoft.WindowsFeedback"
    #"Microsoft.XboxGameCallableUI"
    #"Microsoft.XboxIdentityProvider"
    #"Windows.ContactSupport"

    # 3rd Party
    "*Amazon.com*"
    "*Autodesk*"
    "*BubbleWitch*"
    "*CandyCrush*"
    "*CookingFever*"
    "*Dell*"
    "*DisneyMagicKingdoms"
    "*DragonManiaLegends"
    "*Facebook*"
    "*HeartsDeluxe*"
    "*HiddenCityMysteryofShadows"
    "*HPConnectedPhotopoweredbySnapfish*"
    "*HPFileViewer*"
    "*Keeper*"
    "*LenovoCompanion"
    "*LinkedInforWindows"
    "*MarchofEmpires*"
    "*McAfee*"
    "*Minecraft*"
    "*Netflix*"
    "*Plex*"
    "*SimpleSolitaire*"
    "*TheWeatherChannel*"
    "*TripAdvisor*"
    "*Twitter*"
    "SpotifyAB.SpotifyMusic"
)

foreach ($app in $apps) {
    Write-Output "Trying to remove $app"

    Get-AppxPackage -AllUsers $app | Remove-AppxPackage # --ErrorAction SilentlyContinue

    Get-AppXProvisionedPackage -Online |
        Where-Object DisplayName -EQ $app |
        Remove-AppxProvisionedPackage -Online
}

# Prevents "Suggested Applications" returning
# force-mkdir "HKLM:\SOFTWARE\Policies\Microsoft\Windows\Cloud Content"
# Set-ItemProperty "HKLM:\SOFTWARE\Policies\Microsoft\Windows\Cloud Content" "DisableWindowsConsumerFeatures" 1


# Microsoft IIS
cuninst IIS-WebServerRole -source windowsfeatures
cuninst IIS-WebServer -source windowsfeatures
# cuninst IIS-CommonHttpFeatures -source windowsfeatures
# cuninst IIS-HttpErrors -source windowsfeatures
# cuninst IIS-HttpRedirect -source windowsfeatures
# cuninst IIS-ApplicationDevelopment -source windowsfeatures
# cuninst IIS-NetFxExtensibility -source windowsfeatures
# cuninst IIS-NetFxExtensibility45 -source windowsfeatures
# cuninst IIS-HealthAndDiagnostics -source windowsfeatures
# cuninst IIS-HttpLogging -source windowsfeatures
# cuninst IIS-LoggingLibraries -source windowsfeatures
# cuninst IIS-RequestMonitor -source windowsfeatures
# cuninst IIS-HttpTracing -source windowsfeatures
# cuninst IIS-Security -source windowsfeatures
# cuninst IIS-URLAuthorization -source windowsfeatures
# cuninst IIS-RequestFiltering -source windowsfeatures
# cuninst IIS-IPSecurity -source windowsfeatures
# cuninst IIS-Performance -source windowsfeatures
# cuninst IIS-HttpCompressionDynamic -source windowsfeatures
# cuninst IIS-WebServerManagementTools -source windowsfeatures
# cuninst IIS-ManagementScriptingTools -source windowsfeatures
# cuninst IIS-IIS6ManagementCompatibility -source windowsfeatures
# cuninst IIS-Metabase -source windowsfeatures
# cuninst IIS-HostableWebCore -source windowsfeatures
# cuninst IIS-StaticContent -source windowsfeatures
# cuninst IIS-DefaultDocument -source windowsfeatures
# cuninst IIS-DirectoryBrowsing -source windowsfeatures
# cuninst IIS-WebDAV -source windowsfeatures
# cuninst IIS-WebSockets -source windowsfeatures
# cuninst IIS-ApplicationInit -source windowsfeatures
# cuninst IIS-ASPNET -source windowsfeatures
# cuninst IIS-ASPNET45 -source windowsfeatures
# cuninst IIS-ASP -source windowsfeatures
# cuninst IIS-CGI -source windowsfeatures
# cuninst IIS-ISAPIExtensions -source windowsfeatures
# cuninst IIS-ISAPIFilter -source windowsfeatures
# cuninst IIS-ServerSideIncludes -source windowsfeatures
# cuninst IIS-CustomLogging -source windowsfeatures
# cuninst IIS-BasicAuthentication -source windowsfeatures
# cuninst IIS-HttpCompressionStatic -source windowsfeatures
# cuninst IIS-ManagementConsole -source windowsfeatures
# cuninst IIS-ManagementService -source windowsfeatures
# cuninst IIS-WMICompatibility -source windowsfeatures
# cuninst IIS-LegacyScripts -source windowsfeatures
# cuninst IIS-LegacySnapIn -source windowsfeatures
# cuninst IIS-FTPServer -source windowsfeatures
# cuninst IIS-FTPSvc -source windowsfeatures
# cuninst IIS-FTPExtensibility -source windowsfeatures

# See How to Completely Uninstall OneDrive in Windows 10 http://lifehacker.com/how-to-completely-uninstall-onedrive-in-windows-10-1725363532
taskkill /f /im OneDrive.exe
C:\Windows\SysWOW64\OneDriveSetup.exe /uninstall
