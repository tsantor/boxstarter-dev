# Boxstarter Dev Machine

## Overview
A collection of Powershell scripts to quickly get a new dev machine up and running.

## Usage
## Usage
Open Powershell as an Administrator. CD into the `boxstarter-dev` directory and execute the `install.ps1` script:
```
Set-ExecutionPolicy Unrestricted
.\install-boxstarter.ps1
.\install.ps1
```

## Resources

- http://blog.zerosharp.com/provisioning-a-new-development-machine-with-boxstarter/
- https://gist.github.com/ElJefeDSecurIT/014fcfb87a7372d64934995b5f09683e
- https://gist.github.com/tkrotoff/830231489af5c5818b15
- https://gist.githubusercontent.com/xocasdashdash/2bb1d74adf954a21f343e3c681bf1aad/raw
- https://github.com/JonCubed/boxstarter/blob/master/box.ps1
- https://github.com/robertbird/robertbird.devenvironment/blob/master/MachineBuilds/Boxstarter-Machine-Build.ps1
- https://github.com/W4RH4WK/Debloat-Windows-10
- https://www.askvg.com/guide-how-to-remove-all-built-in-apps-in-windows-10/
