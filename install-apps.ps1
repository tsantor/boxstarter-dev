Write-Output "--------------------------------------------------------------------------------"

# Install a list of packages if present
Write-Output "Installing packages..."
choco feature enable -n allowGlobalConfirmation

# Dev tools
cinst PowerShell --limitoutput
cinst curl --limitoutput
cinst git -params '"/GitAndUnixToolsOnPath /WindowsTerminal"' -y -v --limitoutput
# cinst git-credential-manager-for-windows
# cinst gitkraken
cinst nodejs --limitoutput
cinst python --version 3.6.5
# cinst notepadplusplus --limitoutput
#cinst fiddler4 --limitoutput
cinst commandwindowhere --limitoutput
cinst slack --limitoutput
cinst microsoft-build-tools

# Apps
cinst sublimetext3 --limitoutput
cinst visualstudiocode --limitoutput
cinst googlechrome --limitoutput
# cinst googledrive --limitoutput
cinst dropbox --limitoutput
cinst adobereader --limitoutput
cinst vlc --limitoutput
# cinst gimp --version 2.8.16
# cinst skype --limitoutput

# Other essential junk
cinst 7zip --limitoutput
cinst filezilla --limitoutput
cinst winrar --limitoutput
cinst javaruntime --limitoutput
# cinst lastpass --limitoutput

# pin apps that update themselves
choco pin add -n=googlechrome

# Fonts
cinst inconsolata --limitoutput

# Tools
cinst sysinternals --limitoutput
