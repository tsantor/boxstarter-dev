# Boxstarter options
$Boxstarter.RebootOk=$true # Allow reboots?
$Boxstarter.NoPassword=$false # Is this a machine with no login password?
$Boxstarter.AutoLogin=$true # Save my password securely and auto-login after a reboot

# Allow running PowerShell scripts
Write-Output "Setting execution policy"
Set-ExecutionPolicy Unrestricted
Disable-UAC
# Enable-RemoteDesktop

# Enable Windows Controlled Folder Access
Write-Output "Enable Windows Controlled Folder Access"
Set-MpPreference -EnableControlledFolderAccess Enabled

. ".\windows-update.ps1"
. ".\remove-default-apps.ps1"
. ".\windows-config.ps1"
. ".\install-apps.ps1"
. ".\install-code-extensions.ps1"
. ".\install-wsl.ps1"
#. ".\disable-services.ps1"

#--- Rename the Computer ---
# Requires restart, or add the -Restart flag
# $computername = "xstudios-helios"

# if ($env:computername -ne $computername) {
#    Rename-Computer -NewName $computername
#}

# if (Test-PendingReboot) { Invoke-Reboot }

# Configure windows Hyper-V virtualization
#cinst Microsoft-Hyper-V-All -source windowsFeatures
# if (Test-PendingReboot) { Invoke-Reboot }

# Cleanup
del C:\eula*.txt
del C:\install.*
del C:\vcredist.*
del C:\vc_red.*


#--- List all installed programs --#
# Get-ItemProperty HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall* | sort -property DisplayName | Select-Object DisplayName, DisplayVersion, Publisher, InstallDate |Format-Table -AutoSize

#--- List all store-installed programs --#
# Get-AppxPackage | sort -property Name | Select-Object Name, PackageFullName, Version | Format-Table -AutoSize
